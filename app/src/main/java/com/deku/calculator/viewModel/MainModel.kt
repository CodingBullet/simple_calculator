package com.deku.calculator.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import net.objecthunter.exp4j.ExpressionBuilder
import java.lang.Exception

class MainModel : ViewModel() {
    private val _expression = MutableLiveData("0")
    private val _result = MutableLiveData("")
    private val _resultVisibilityGone = MutableLiveData(true)
    private val operator = "+-*/"
    private var canUseDecimal = true
    private var equalPressed = false

    val resultVisibilityGone: LiveData<Boolean> = _resultVisibilityGone
    val expression: LiveData<String> = _expression
    val result: LiveData<String> = _result

    fun clear() {
        if (expression.value!!.last() == '.') canUseDecimal = true

        if (_expression.value!!.length > 1) {
            _expression.value = _expression.value!!.dropLast(1)
        } else {
            _expression.value = "0"
        }
    }

    fun clearAll() {
        _expression.value = "0"
        _resultVisibilityGone.value = true
        canUseDecimal = true
    }

    fun appendExpression(char: String) {
        if (equalPressed) {
            _expression.value = _result.value
            equalPressed = false
        }
        _expression.value = "${_expression.value}$char"
    }

    fun addOperator(char: String) {
        if (operator.contains(_expression.value!!.last())) {
            _expression.value = _expression.value!!.dropLast(1)
        }
        canUseDecimal = true
        appendExpression(char)
        _resultVisibilityGone.value = false
    }

    fun addValue(char: String) {
        if (_expression.value == "0") {
            _expression.value = ""
        }
        appendExpression(char)
    }

    fun addDecimal() {
        if (canUseDecimal) {
            appendExpression(".")
            canUseDecimal = false
        }
    }

    fun calculateResult() {
        try {
            if (!(operator.contains(_expression.value!!.last()))) {
                val calculatedResult = ExpressionBuilder(_expression.value).build().evaluate()
                if (!calculatedResult.isNaN()) {
                    val longResult = calculatedResult.toLong()
                    if (calculatedResult == longResult.toDouble()) {
                        _result.value = longResult.toString()
                    } else {
                        _result.value = calculatedResult.toString()
                    }
                }
            }
        } catch (e: Exception) {
        }
    }

    fun onEqualPressed() {
        equalPressed = true
    }

}