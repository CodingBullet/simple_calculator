package com.deku.calculator

import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import com.deku.calculator.databinding.ActivityMainBinding
import com.deku.calculator.viewModel.MainModel

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var viewModel: MainModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.tvResult.visibility = View.GONE

        viewModel = ViewModelProvider(this)[MainModel::class.java]
        viewModel.expression.observe(this) { exp ->
            binding.tvExpression.text = exp
            viewModel.calculateResult()
        }
        viewModel.result.observe(this) { res -> binding.tvResult.text = res }
        binding.btnClear.setOnLongClickListener {
            viewModel.clearAll()
            true
        }
        viewModel.resultVisibilityGone.observe(this) { gone ->
            when (gone) {
                true -> binding.tvResult.visibility = View.GONE
                false -> binding.tvResult.visibility = View.VISIBLE
            }
        }

    }

    fun onClick(view: View) {
        if (binding.tvResult.currentTextColor ==
            ContextCompat.getColor(
                this,
                R.color.number_color
            )
        ) {
            binding.tvResult.textSize = MIN_TXT_SIZE
            binding.tvExpression.textSize = MAX_TXT_SIZE
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                binding.tvResult.setTextColor(ContextCompat.getColor(this, R.color.grey))
                binding.tvExpression.setTextColor(
                    ContextCompat.getColor(
                        this,
                        R.color.number_color
                    )
                )
            } else {
                binding.tvResult.setTextColor(resources.getColor(R.color.grey))
                binding.tvExpression.setTextColor(resources.getColor(R.color.number_color))
            }
        }

        when (view) {
            binding.btnZero -> viewModel.addValue("0")
            binding.btnOne -> viewModel.addValue("1")
            binding.btnTwo -> viewModel.addValue("2")
            binding.btnThree -> viewModel.addValue("3")
            binding.btnFour -> viewModel.addValue("4")
            binding.btnFive -> viewModel.addValue("5")
            binding.btnSix -> viewModel.addValue("6")
            binding.btnSeven -> viewModel.addValue("7")
            binding.btnEight -> viewModel.addValue("8")
            binding.btnNine -> viewModel.addValue("9")
            binding.btnZero -> viewModel.addValue("0")
            binding.btnAdd -> viewModel.addOperator("+")
            binding.btnSub -> viewModel.addOperator("-")
            binding.btnMul -> viewModel.addOperator("*")
            binding.btnDiv -> viewModel.addOperator("/")
            binding.btnBracketOpen -> viewModel.appendExpression("(")
            binding.btnBracketClose -> viewModel.appendExpression(")")
            binding.btnClear -> viewModel.clear()
            binding.btnDec -> viewModel.addDecimal()
        }

    }


    fun onEqualClick(view: View) {
        binding.tvResult.textSize = MAX_TXT_SIZE
        binding.tvExpression.textSize = MIN_TXT_SIZE


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            binding.tvResult.setTextColor(ContextCompat.getColor(this, R.color.number_color))
            binding.tvExpression.setTextColor(ContextCompat.getColor(this, R.color.grey))
        } else {
            binding.tvResult.setTextColor(resources.getColor(R.color.number_color))
            binding.tvExpression.setTextColor(resources.getColor(R.color.grey))
        }
        viewModel.onEqualPressed()
    }

    companion object {
        private const val MAX_TXT_SIZE = 42f
        private const val MIN_TXT_SIZE = 36f
    }
}











